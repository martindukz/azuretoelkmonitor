﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.Management.Monitor;
using Microsoft.Azure.Management.Monitor.Models;
using Microsoft.Rest.Azure.Authentication;
using Microsoft.Rest.Azure.OData;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.Elasticsearch;

namespace AzureToElkMonitor
{
    class Program
    {
        private static MonitorClient readOnlyClient;

        private static string logstash = "logstash-{0:yyyy.MM}";

        static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo(0x0809);
            var loggerConfig = new LoggerConfiguration()
                .MinimumLevel.Debug();
            var logLines = File.ReadAllLines(@"c:/temp/azuretoelk/loggingInfo.txt");
            var uri = new Uri(logLines[0]);
            var bufferBaseFilename = Path.Combine(@"c:\d60", logLines[1], "es", "log");
            uri = new UriBuilder(uri.Scheme, uri.Host, uri.Port)
            {
                UserName = logLines[1],
                Password = logLines[2]
            }.Uri;
            loggerConfig.WriteTo.Elasticsearch(new ElasticsearchSinkOptions(uri)
            {
                AutoRegisterTemplate = true,
                BufferBaseFilename = bufferBaseFilename,
                MinimumLogEventLevel = LogEventLevel.Verbose,
                IndexFormat = logstash
            });


            var logger = loggerConfig.CreateLogger().ForContext("SourceContext", logLines[1]);
            Log.Logger = logger;
            //https://docs.microsoft.com/en-us/azure/monitoring-and-diagnostics/monitoring-supported-metrics
            //https://github.com/Azure-Samples/monitor-dotnet-metrics-api/blob/master/README.md
            //https://docs.microsoft.com/da-dk/azure/azure-resource-manager/resource-group-create-service-principal-portal
            // Also open cloud explorer in visual studio.
            //Martindukz user 
            //Tilføjede TestAzureToElkAccess app til rolle: API Management service reader role
            var lines = File.ReadAllLines(@"c:/temp/azuretoelk/AzureToElkInput.txt");

            var tenantId = lines[0];
            var clientId = lines[1]; 
            var secret = lines[2];
            var subscriptionId = lines[3];
            
            if (new List<string> { tenantId, clientId, secret, subscriptionId }.Any(i => String.IsNullOrEmpty(i)))
            {
                Console.WriteLine("Please provide environment variables for AZURE_TENANT_ID, AZURE_CLIENT_ID, AZURE_CLIENT_SECRET and AZURE_SUBSCRIPTION_ID.");
            }
            else
            {
                for (int lineIndex = 4; lineIndex < lines.Length; lineIndex++)
                {
                    string resourceId = null;

                    readOnlyClient = AuthenticateWithReadOnlyClient(tenantId, clientId, secret, subscriptionId).Result;
                    try
                    {   
                        resourceId = lines[lineIndex];
                        
                        var resourceUri= $"/subscriptions/{subscriptionId}/" + resourceId;
                        Console.WriteLine("......................................................");
                        Console.WriteLine("Handling resource: " + resourceId);
                        List<MetricDefinition> metricDefs = RunMetricDefinitionsSample(readOnlyClient, resourceUri).Result;
                        RunMetricsSample(readOnlyClient, resourceUri, metricDefs).Wait();

                        Console.WriteLine("Successfully exported: " + resourceId);
                    }
                    catch (Exception ex)
                    {
                        //The web app throws exception because of faulty api version
                        //How to fix? 
                        Console.WriteLine("Resource failed....");
                        Log.Warning(ex, "Resource failed: {resourceId}", resourceId);
                    }
                }
            }
            Console.WriteLine();
            Console.WriteLine("####################################################");
            Console.WriteLine();
            Console.WriteLine("Done exporting.");

            Thread.Sleep(10000);
        }

        #region Authentication

        private static async Task<MonitorClient> AuthenticateWithReadOnlyClient(string tenantId, string clientId,
            string secret, string subscriptionId)
        {
            // Build the service credentials and Monitor client
            var serviceCreds = await ApplicationTokenProvider.LoginSilentAsync(tenantId, clientId, secret);
            var monitorClient = new MonitorClient(serviceCreds);
            monitorClient.SubscriptionId = subscriptionId;

            return monitorClient;
        }

        #endregion

        private static async Task<List<MetricDefinition>> RunMetricDefinitionsSample(MonitorClient readOnlyClient, string resourceUri)
        {
            
            // Get metrics definitions
            IEnumerable<MetricDefinition> metricDefinitions = await readOnlyClient.MetricDefinitions.ListAsync(resourceUri: resourceUri, cancellationToken: new CancellationToken());

            var definitions = metricDefinitions as IList<MetricDefinition> ?? metricDefinitions.ToList();
            EnumerateMetricDefinitions(definitions);

            return definitions.ToList();
        }

        #region Examples

        private static async Task RunMetricsSample(MonitorClient readOnlyClient, string resourceUri, List<MetricDefinition> definitions)
        {
            foreach (var def in definitions)
            {
                try
                {
                    var defValue = def.Name.Value; //"Network In";
                    var start = DateTime.UtcNow.AddHours(-3);
                    var end = DateTime.UtcNow;
                    var startTime = start.ToString("s") + "Z";
                    var endTime = end.ToString("s") + "Z";
                    var durationThing = "PT5M"; //def.
                    var filterString =
                        $"name.value eq '{defValue}' and timeGrain eq duration'{durationThing}' and startTime eq {startTime} and endTime eq {endTime} ";
                    var odataFilterMetrics = new ODataQuery<Metric>(filterString);

                    Write("Call with filter parameter (i.e. $filter = {0})", odataFilterMetrics);
                    var metrics = (await readOnlyClient.Metrics.ListAsync(resourceUri: resourceUri,
                        odataQuery: odataFilterMetrics, cancellationToken: CancellationToken.None)).ToList();
                    EnumerateMetrics(metrics);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Metric failed for " + def.Name.Value + " with message: " + ex.Message);
                }
            }
        }

        #endregion

        #region Helpers

        private static void Write(string format, params object[] items)
        {
            Console.WriteLine(string.Format(format, items));
        }

        private static void EnumerateMetricDefinitions(IEnumerable<MetricDefinition> metricDefinitions)
        {
            foreach (var metricDefinition in metricDefinitions)
            {
                Console.WriteLine("Found definition: " + metricDefinition.Name.Value); 
            }
        }

        private static void EnumerateMetrics(IEnumerable<Metric> metrics, int maxRecords = 5)
        {
            foreach (var metric in metrics)
            {
                var numRecords = 0;
                Console.WriteLine("Exporting data for " + metric.Name.Value);
                foreach (var dataItem in metric.Data)
                {
                    Log.Logger.Information(
                        "{kpi} - {metric_id}: {metric_namevalue}, {metric_type}, {metric_unit}, {metric_datavalue_avg}, {metric_datavalue_max}, {metric_datavalue_min}, {metric_datavalue_total} {metric_datavalue_count} - at {metric_timestamp}",
                        "metricKpi",
                        metric.Id,
                        metric.Name.Value,
                        metric.Type,
                        metric.Unit,
                        dataItem.Average,
                        dataItem.Maximum,
                        dataItem.Minimum,
                        dataItem.Total,
                        dataItem.Count,
                        dataItem.TimeStamp
                    );

                    // Display only 5 records at most
                    numRecords++;
                    if (numRecords >= maxRecords)
                    {
                        break;
                    }
                }
            }
        }

        #endregion

        // Define other methods and classes here

    }
}
